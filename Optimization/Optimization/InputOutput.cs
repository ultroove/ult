﻿using System;
using System.Collections.Generic;

namespace Optimization
{
    class InputOutput
    {
        public static int SafeIntInputWithRange(string inviteMessage, uint minValue, uint maxValue)
        {
            bool inputFlag, rangeFlag;
            int intValue;
            string getLine;

            do
            {
                do
                {
                    Console.Write(inviteMessage);
                    getLine = Console.ReadLine();

                    if (!(inputFlag = int.TryParse(getLine, out intValue)))
                        Console.WriteLine("Ошибка! Введённое значение не является целым числом. Повторите ввод.");
                } while (!inputFlag);

                if (intValue < minValue || intValue > maxValue)
                {
                    Console.WriteLine("Ошибка! Значение должно быть в диапазоне [{0}..{1}]. Повторите ввод.", minValue, maxValue);
                    rangeFlag = false;
                }
                else
                    rangeFlag = true;
            } while (!rangeFlag);
            

            return intValue;
        }

        public static double SafeDoubleInputWithRange(string inviteMessage, double minValue, double maxValue)
        {
            bool inputFlag, rangeFlag;
            double doubleValue;
            string getLine;

            do
            {
                do
                {
                    Console.Write(inviteMessage);
                    getLine = Console.ReadLine();

                    if (!(inputFlag = double.TryParse(getLine, out doubleValue)))
                        Console.WriteLine("Ошибка! Введённое значение не является вещественным числом. Повторите ввод.");
                } while (!inputFlag);

                if (doubleValue < minValue || doubleValue > maxValue)
                {
                    Console.WriteLine("Ошибка! Значение должно быть в диапазоне [{0}..{1}]. Повторите ввод.", minValue, maxValue);
                    rangeFlag = false;
                }
                else
                    rangeFlag = true;
            } while (!rangeFlag);
            return doubleValue;
        }

        public static void PrintResult(List<double> x, double Q)
        {
            Console.WriteLine("Результаты оптимизации:");
            for (var i = 0; i < x.Count; i++)
                Console.WriteLine("x{0} = {1:F3}", i + 1, x[i]);
                    
            Console.WriteLine("Q = {0:F5}", Q);
        }

        public static void PrintVector(List<double> x, string name)
        {
            Console.Write(name+" = (");
            for (var i = 0; i < x.Count - 1; i++)
            {
                Console.Write("{0}; ", x[i]);
            }
            Console.WriteLine("{0})", x[x.Count-1]);
        }

        public static bool YesOrNo(string message)
        {
            Console.Write(message);
            var a = Console.ReadKey();
            Console.WriteLine();
            return a.Key == ConsoleKey.Enter;
        }
    }
}
