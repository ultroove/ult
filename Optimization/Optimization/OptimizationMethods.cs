﻿using System;
using System.Collections.Generic;

namespace Optimization
{
    class OptimizationMethods
    {
        private static List<double> S = new List<double>();  //вектор перемещения в пространстве независимых переменных
        private static double M = 1;  //масштаб
        private static bool lastStepIsBad;  //флаг неудачности последнего шага
        private static int limitGoodSteps = 0;  //лимитирующий счётчик удачных шагов
        private static int delay4increasing = 2; //число удачных шагов до начала увеличения рабочего шага поиска
        private static int stepCounter; //общий счётчик шагов


        public delegate List<double> MethodDelegate(TestFunctions.FunctionDelegate Q, List<double> startPoint,
            double minX, double maxX, double step, int stepQty, bool adaptStep, double gamma1, double gamma2, out double Qopt, out double aLast);
        //объявление делегата

        public static MethodDelegate GetMethod(int a)
        {
            var methods = new  MethodDelegate[] { GaussSeidelMethod, GradientMethod, SteepestDescentMethod,
                RandomWithReturnMethod, RandomWithConversionMethod, RandomMixedTacticMethod };
            return methods[a];
        }
        //получение нужного метода по номеру

        private static int num = 0;
        //номер аргумента для метода покоординатного спуска

        private static void CopyVector(List<double> inputVector, List<double> outputVector)
        {
            outputVector.Clear();
            foreach (var t in inputVector)
            {
                outputVector.Add(t);
            }
        }
        //создание копии вектора

        private static void MakeAStep(List<double> xIn, double a, List<double> sIn, double scale)
        {
            for (var i = 0; i < xIn.Count; i++)
                xIn[i] += a*sIn[i]*scale;
        }
        //перемещение по вектору в пространстве независимых переменных

        private static void CheckRange(List<double> x, double xMin, double xMax)
        {
            for (var i = 0; i < x.Count; i++)
            {
                /*if (x[i] < xMin[i]) x[i] = xMin[i];
                if (x[i] > xMax[i]) x[i] = xMax[i];*/
                if (x[i] < xMin) x[i] = xMin;
                if (x[i] > xMax) x[i] = xMax;
            }
        }
        //проверка ограничений

        private static bool CheckExact(List<double> x, TestFunctions.FunctionDelegate f)
        {
            var xPlus = new List<double>();
            var xMinus = new List<double>();
            const double delta = 0.0001;
            bool result = true;

            for (var i = 0; i < x.Count; i++)
            {
                xPlus.Add(x[i]);
                xMinus.Add(x[i]);
            }

            for (var i = 0; i < x.Count; i++)
            {
                xPlus[i] += delta;
                xMinus[i] -= delta;
                if ((f(xPlus) < f(x)) || (f(xMinus) < f(x)))
                {
                    result = false;
                    break;
                }
                else
                {
                    xPlus[i] = x[i];
                    xMinus[i] = x[i];
                }

            }

            if (result)
                Console.WriteLine("\nНайдено точное решение!\n");

            return result;
        }
        //проверка точного решения

        private static List<double> UnitVector(List<double> x, TestFunctions.FunctionDelegate f, int n)
        {
            var vector = new List<double>();
            var xPlus = new List<double>();
            const double delta = 0.0001;

            for (var i = 0; i < x.Count; i++)
            {
                xPlus.Add(x[i]);
                vector.Add(0);
            }

            xPlus[n] += delta;
            if (f(xPlus) < f(x))
                vector[n] = 1;
            else
                vector[n] = -1;

            return vector;
        }

        private static List<double> AntiGradientVector(List<double> x, TestFunctions.FunctionDelegate f)
        {
            var xPlus = new List<double>();
            var xMinus = new List<double>();
            var vector = new List<double>();
            double modulus = 0;
            const double delta = 0.0001;
            for (var i = 0; i < x.Count; i++)
            {
                xPlus.Clear();
                xMinus.Clear();

                foreach (var t in x)
                {
                    xPlus.Add(t);
                    xMinus.Add(t);
                }

                xPlus[i]+=delta;
                xMinus[i]-=delta;
                
                vector.Add(-(f(xPlus)-f(xMinus))/(2*delta));

                modulus += vector[i] * vector[i];
            }

            for (var i = 0; i < vector.Count; i++)
            {
                vector[i] /= Math.Sqrt(modulus);
            }

            return vector;
        }

        private static Random r = new Random();

        private static List<double> RandomVector(int size)
        {
            //var r = new Random();
            var vector = new List<double>();
            
            for (var i = 0; i < size; i++)
                vector.Add(-1 + 2 * r.NextDouble());

            
            //InputOutput.PrintVector(vector);
            return vector;
        }

        public static List<double> GaussSeidelMethod(TestFunctions.FunctionDelegate Q, List<double> startPoint,
            double minX, double maxX, double step, int stepQty, bool adaptStep, double gamma1, double gamma2, out double Qopt, out double aLast)
        {
            List<double> x = new List<double>();
            List<double> prevX = new List<double>();
            stepCounter = 0;
            //int badStepsCounter = 0;
            //int goodStepsCounter = 0;
            
            CopyVector(startPoint, x);
           // M = (maxX.Max() - minX.Min()) / 2;
            M = (maxX - minX) / 2;

            do
            {
                for (var i = num % x.Count; i < x.Count; i++)
                {
                    CopyVector(UnitVector(x, Q, i), S);
                    //InputOutput.PrintVector(S);

                    do
                    {
                        CopyVector(x, prevX);
                        MakeAStep(x, step, S, M);
                        CheckRange(x, minX, maxX);
                        stepCounter++;


                        if (Q(x) < Q(prevX))
                        {
                            limitGoodSteps++;
                            lastStepIsBad = false;
                        }
                            

                        if ((adaptStep) && (limitGoodSteps > delay4increasing))
                        {
                            step *= gamma1;
                        }

                        //InputOutput.PrintVector(S);
                        //step *= gamma1;

                    } while ((Q(x) < Q(prevX)) && (stepCounter < stepQty));

                    if (Q(x) >= Q(prevX))
                    {
                        CopyVector(prevX, x);
                        lastStepIsBad = true;
                        stepCounter--;
                        limitGoodSteps = 0;
                        num++;
                    }

                    

                    if (stepCounter >= stepQty)
                    {
                        break;
                    }
                        
                }
                //stepCounter++;
                if (lastStepIsBad)
                    step *= gamma2;

                if (CheckExact(x, Q))
                    break;
                

            } while (stepCounter<stepQty);                        
            Qopt = Q(x);
            aLast = step;
            
            return x;
        }

        public static List<double> GradientMethod(TestFunctions.FunctionDelegate Q, List<double> startPoint,
            double minX, double maxX, double step, int stepQty, bool adaptStep, double gamma1, double gamma2, out double Qopt, out double aLast)
        {
            List<double> x = new List<double>();
            List<double> prevX = new List<double>();

            //gamma2 = 0.8;
            stepCounter = 0;
            
            CopyVector(startPoint, x);
            //M = (maxX.Max() - minX.Min()) / 2;
            M = (maxX - minX)/2;

            do
            {
                CopyVector(AntiGradientVector(x, Q), S);
                CopyVector(x, prevX);

                //MakeAStep(x, step, S, M);
                for (var i = 0; i < x.Count; i++)
                    x[i] += step * S[i] * M;

                CheckRange(x, minX, maxX);
                if (Q(x) >= Q(prevX))
                {
                    step *= gamma2;
                    lastStepIsBad = true;
                    limitGoodSteps = 0;
                }
                else
                {
                    lastStepIsBad = false;
                    limitGoodSteps++;
                }

                if ((adaptStep) && (limitGoodSteps > delay4increasing))
                {
                    step *= gamma1;
                }

                stepCounter++;

                if (CheckExact(x, Q))
                    break;

            } while (stepCounter<stepQty);

            if (lastStepIsBad) //если последний шаг был неудачным
                CopyVector(prevX, x); //возвращаем предыдущий x, а не текущий
            
            Qopt = Q(x);
            aLast = step;
            return x;
        }

        public static List<double> SteepestDescentMethod(TestFunctions.FunctionDelegate Q, List<double> startPoint,
            double minX, double maxX, double step, int stepQty, bool adaptStep, double gamma1, double gamma2, out double Qopt, out double aLast)
        {
            List<double> x = new List<double>();
            List<double> prevX = new List<double>();
            //gamma1 = 1.1;
            //gamma2 = 0.9;
            M = (maxX - minX) / 2;
            stepCounter = 0;
            CopyVector(startPoint, x);

            do
            {
                CopyVector(AntiGradientVector(x, Q), S);
                do
                {
                    CopyVector(x, prevX);

                    //MakeAStep(x, step, S, M);
                    for (var i = 0; i < x.Count; i++)
                        x[i] += step*S[i]*M;

                    CheckRange(x, minX, maxX);
                    stepCounter++;
                    if (Q(x) >= Q(prevX))
                    {
                        step *= gamma2;
                        lastStepIsBad = true;
                        limitGoodSteps = 0;
                    }
                    else
                    {
                        lastStepIsBad = false;
                        limitGoodSteps++;
                    }

                    if ((adaptStep) && (limitGoodSteps > delay4increasing))
                    {
                        step *= gamma1;
                    }


                } while ((Q(x)<Q(prevX)) && (stepCounter < stepQty));

                if (CheckExact(x, Q))
                    break;

            } while (stepCounter < stepQty);

            if (lastStepIsBad) //если последний шаг был неудачным
                CopyVector(prevX, x); //возвращаем предыдущий x, а не текущий

            Qopt = Q(x);
            aLast = step;
            return x;
        }

        public static List<double> RandomWithReturnMethod(TestFunctions.FunctionDelegate Q, List<double> startPoint,
            double minX, double maxX, double step, int stepQty, bool adaptStep, double gamma1, double gamma2, out double Qopt, out double aLast)
        {
            List<double> x = new List<double>(); //текущая точка
            List<double> prevX = new List<double>(); //предыдущая точка
            stepCounter = 0; //счётчик шагов
            int badStepsCounter = 0;  //счётчик неудачных шагов
            int goodStepsCounter = 0; //счётчик удачных шагов
            int limitBadSteps = 0;  // лимитирующий счётчик неудачных шагов
            CopyVector(startPoint, x); //запись начальной точки в x
            M = (maxX - minX) / 2; //установка масштаба

            do
            {
                CopyVector(RandomVector(x.Count), S); //запись в S случайного вектора
                
                CopyVector(x, prevX); //запоминание текущей точки
                MakeAStep(x, step, S, M); //выполнение перемещения в направлении S
                CheckRange(x, minX, maxX); //проверка ограничений
                stepCounter++; //увеличение счётчика шагов
                if (Q(x) >= Q(prevX)) //если шаг неудачный
                {
                    CopyVector(prevX, x); //вернуться в предыдущую точку

                    badStepsCounter++;

                    limitBadSteps++; //увеличение счётчика неудачных шагов

                    limitGoodSteps = 0; 
                }
                else //если шаг удачный
                {
                    limitBadSteps = 0; //обнуление счётчика неудачных шагов

                    goodStepsCounter++;
                    limitGoodSteps++;
                }

                if (limitBadSteps > stepQty/5) //если неудачных шагов больше пяти
                    step *= gamma2; //уменьшить шаг

                if ((adaptStep) && (limitGoodSteps > delay4increasing))
                {
                    step *= gamma1;
                }

                if (CheckExact(x, Q))
                    break;

            } while (stepCounter<stepQty); //пока счётчик шагов не достиг предела

            Qopt = Q(x);
            aLast = step;

            Console.WriteLine("Удачных шагов: {0}, неудачных шагов: {1}", goodStepsCounter, badStepsCounter);

            return x;
        }

        public static List<double> RandomWithConversionMethod(TestFunctions.FunctionDelegate Q, List<double> startPoint,
            double minX, double maxX, double step, int stepQty, bool adaptStep, double gamma1, double gamma2, out double Qopt, out double aLast)
        {
            List<double> x = new List<double>();
            List<double> prevX = new List<double>();
            stepCounter = 0;
            int badStepsCounter = 0;
            int goodStepsCounter = 0;
            int limitBadSteps = 0;
            CopyVector(startPoint, x);
            //M = (maxX.Max() - minX.Min()) / 2;
            M = (maxX - minX) / 2;

            do
            {

                CopyVector(RandomVector(x.Count), S);

                CopyVector(x, prevX);
                MakeAStep(x, step, S, M);
                CheckRange(x, minX, maxX);
                stepCounter++;
                if (Q(x) >= Q(prevX))
                {
                    CopyVector(prevX, x);
                    MakeAStep(x, -step, S, M);
                    CheckRange(x, minX, maxX);
                    if (Q(x) >= Q(prevX))
                    {
                        CopyVector(prevX, x);
                        step *= gamma2;
                        badStepsCounter++;
                        limitBadSteps++;
                        limitGoodSteps = 0;
                    }
                    else
                    {
                        limitBadSteps = 0;
                        goodStepsCounter++;
                        limitGoodSteps++;
                    }
                        
                }
                else
                {
                    limitBadSteps = 0;
                    goodStepsCounter++;
                    limitGoodSteps++;
                }

                if (limitBadSteps > stepQty/5)
                    step *= gamma2;

                if ((adaptStep) && (limitGoodSteps > delay4increasing))
                {
                    step *= gamma1;
                }

                if (CheckExact(x, Q))
                    break;


            } while (stepCounter < stepQty);

            Qopt = Q(x);
            aLast = step;

            Console.WriteLine("Удачных шагов: {0}, неудачных шагов: {1}", goodStepsCounter, badStepsCounter);

            return x;
        }

        public static List<double> RandomMixedTacticMethod(TestFunctions.FunctionDelegate Q, List<double> startPoint,
            double minX, double maxX, double step, int stepQty, bool adaptStep, double gamma1, double gamma2, out double Qopt, out double aLast)
        {
            List<double> x = new List<double>();
            List<double> prevX = new List<double>();
            //gamma2 = 0.8;
            //gamma1 = 1.2;
            stepCounter = 0;
            int badStepsCounter = 0;
            int goodStepsCounter = 0;
            int limitBadSteps = 0;
            CopyVector(startPoint, x);
            //M = (maxX.Max() - minX.Min()) / 2;
            M = (maxX - minX) / 2;

            do
            {
                if (Q(x) > Program.Q1)
                {
                    //Console.WriteLine("Шаг {0}, стадия 1", stepCounter);
                    CopyVector(RandomVector(x.Count), S);

                    CopyVector(x, prevX);
                    MakeAStep(x, step, S, M);
                    CheckRange(x, minX, maxX);
                    stepCounter++;
                    if (Q(x) >= Q(prevX))
                    {
                        CopyVector(prevX, x);
                        MakeAStep(x, -step, S, M);
                        CheckRange(x, minX, maxX);
                        if (Q(x) >= Q(prevX))
                        {
                            CopyVector(prevX, x);
                            step *= gamma2;
                            badStepsCounter++;
                            limitBadSteps++;
                            limitGoodSteps = 0;
                        }
                        else
                        {
                            limitBadSteps = 0;
                            goodStepsCounter++;
                            limitGoodSteps++;
                        }

                    }
                    else
                    {
                        limitBadSteps = 0;
                        goodStepsCounter++;
                        limitGoodSteps++;
                    }

                    if (limitBadSteps > stepQty / 5)
                        step *= gamma2;

                    if ((adaptStep) && (limitGoodSteps > delay4increasing))
                    {
                        step *= gamma1;
                    }
                }
                else
                {
                    var littleStep = step/1;
                    bool continueRunning;
                    do
                    {
                        CopyVector(RandomVector(x.Count), S);
                        //Console.WriteLine("Шаг {0}, стадия 2", stepCounter);
                        continueRunning = true;
                        do
                        {
                            CopyVector(x, prevX);
                            MakeAStep(x, littleStep, S, M);
                            CheckRange(x, minX, maxX);
                            stepCounter++;
                            if (Q(x) >= Q(prevX))
                            {
                                continueRunning = false;
                                CopyVector(prevX, x);
                                badStepsCounter++;
                                limitBadSteps++;
                                limitGoodSteps = 0;
                                //littleStep *= gamma2;
                            }
                            else
                            {
                                goodStepsCounter++;
                                limitBadSteps = 0;
                                limitGoodSteps++;
                                //littleStep *= gamma1;
                            }

                            if (limitBadSteps > stepQty/5) littleStep *= gamma2;

                            if ((adaptStep) && (limitGoodSteps > delay4increasing))
                            {
                                step *= gamma1;
                            }


                        } while (continueRunning && stepCounter<stepQty);
                    } while (continueRunning && stepCounter < stepQty);

                    step = littleStep*1;
                    //Console.WriteLine("Стадия II окончена.");
                }

                if (CheckExact(x, Q))
                    break;

            } while (stepCounter < stepQty);

            Qopt = Q(x);
            aLast = step;

            Console.WriteLine("Удачных шагов: {0}, неудачных шагов: {1}", goodStepsCounter, badStepsCounter);

            return x;
        }


    }
}

