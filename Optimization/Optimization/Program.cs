﻿using System;
using System.Linq;

namespace Optimization
{
    class Program
    {
        static void Main(string[] args)
        {
            const double min4user = 0.001;

            double Qopt;
            

            Console.WriteLine("Выберите тестовую функцию из списка\n(Описание функций см. в файле Readme.docx или в учебно-методическом пособии\n\"Поисковые алгоритмы оптимизации в решении задач ТАУ\"):");
            Console.WriteLine("1. Парабола (две независимые переменные);");
            Console.WriteLine("2. Парабола (три независимые переменные);");
            Console.WriteLine("3. Функция Розенброка;");
            Console.WriteLine("4. Асимметричная долина;");
            Console.WriteLine("5. Функция Пауэлла;");
            Console.WriteLine("6. Квадратичная функция 10 переменных;");
            var n = InputOutput.SafeIntInputWithRange("Введите номер нужной функции [1..6]: ", 1, 6);

            Console.WriteLine("Начальная точка и границы по умолчанию:");

            var startPoint = TestFunctions.GetDefaultPoint(n - 1);
            InputOutput.PrintVector(startPoint, "x");
            double Xmin = -10;
            double Xmax = 100;
            Console.WriteLine("x min = {0}; x max = {1}", Xmin, Xmax);
            if (!(InputOutput.YesOrNo("Оставить значения по умолчанию? Да - <Enter>, нет - любая другая клавиша: ")))
            {
                Console.WriteLine("Введите координаты начальной точки:");
                for (var i = 0; i < startPoint.Count; i++)
                    startPoint[i] = InputOutput.SafeDoubleInputWithRange("x" + (i + 1) + "= ", double.MinValue, double.MaxValue);
                Console.WriteLine("Введите границы:");
                Xmin = InputOutput.SafeDoubleInputWithRange("x min = ", double.MinValue, startPoint.Min());
                Xmax = InputOutput.SafeDoubleInputWithRange("x max = ", startPoint.Max(), double.MaxValue);
            }

            /*var Xmin = new List<double>();
            var Xmax = new List<double>();
            
            for (var i = 0; i < TestFunctions.GetMeasure(n - 1); i++)
            {
                Xmin.Add(-1000);
                Xmax.Add(1000);
            }*/
            

            //if (!(InputOutput.YesOrNo("Нажмите <Enter>, чтобы продолжить с границами по умолчанию,\nили любую другую клавишу для ввода границ: ")))
            
                /*for (var i = 0; i < TestFunctions.GetMeasure(n - 1); i++)
                {
                    Xmin[i] = InputOutput.SafeDoubleInputWithRange("x" + (i + 1) + " min : ", double.MinValue, Double.MaxValue)-min4user;
                    Xmax[i] = InputOutput.SafeDoubleInputWithRange("x" + (i + 1) + " max : ", Xmin[i]+min4user, double.MaxValue);
                }*/

            Console.WriteLine("Выберите метод оптимизации из списка:");
            Console.WriteLine("1. Метод покоординатного спуска (Гаусса-Зайделя);");
            Console.WriteLine("2. Метод градиента;");
            Console.WriteLine("3. Метод наискорейшего спуска;");
            Console.WriteLine("4. Метод случайного поиска с возвратом;");
            Console.WriteLine("5. Метод случайного поиска с пересчётом;");
            Console.WriteLine("6. Метод случайного поиска со смешанной тактикой;");

            var m = InputOutput.SafeIntInputWithRange("Введите номер нужного метода [1..6]: ", 1, 6);

            double a = 1;
            Console.WriteLine("Начальное значение шага поиска по умолчанию: a = {0}", a);
            if (
                !(InputOutput.YesOrNo(
                    "Оставить значение по умолчанию? Да - <Enter>, нет - любая другая клавиша: ")))
            {
                a = InputOutput.SafeDoubleInputWithRange("a = ", min4user, double.MaxValue);
            }

            bool increaseStep = (InputOutput.YesOrNo(
                    "Использовать адаптацию шага? Да - <Enter>, нет - любая другая клавиша: "));

            double gamma1 = 1.2;
            double gamma2 = 0.8;
            if (increaseStep)
            {
                Console.WriteLine("Коэффициенты адаптации по умолчанию: gamma1 = {0}, gamma2 = {1}", gamma1, gamma2);
                if (
                !(InputOutput.YesOrNo(
                    "Оставить значения по умолчанию? Да - <Enter>, нет - любая другая клавиша: ")))
                {
                    gamma1 = InputOutput.SafeDoubleInputWithRange("gamma1 = ", 1, double.MaxValue);
                    gamma2 = InputOutput.SafeDoubleInputWithRange("gamma2 = ", 0, 1);
                }
            }


            var s = InputOutput.SafeIntInputWithRange("Введите количество шагов: ", 1, int.MaxValue);
            if (m == 6)
            {
                Q1 = InputOutput.SafeDoubleInputWithRange("Введите значение границы смены тактики (Q1): ",
                    double.MinValue, double.MaxValue);
            }

            var endPoint = OptimizationMethods.GetMethod(m - 1)(TestFunctions.GetFunction(n - 1), startPoint, Xmin, Xmax, a, s,
                increaseStep, gamma1, gamma2, out Qopt, out a);
            InputOutput.PrintResult(endPoint, Qopt);

            while (InputOutput.YesOrNo("Нажмите клавишу <Enter>, чтобы продолжить оптимизацию с текущими параметрами,\nили любую другую клавишу для выхода: "))
            {
                endPoint = OptimizationMethods.GetMethod(m - 1)(TestFunctions.GetFunction(n - 1), endPoint, Xmin, Xmax, a, s,
                    increaseStep, gamma1, gamma2, out Qopt, out a);
                InputOutput.PrintResult(endPoint, Qopt);
            }

        }

        public static double Q1 = new double();



    }

}
