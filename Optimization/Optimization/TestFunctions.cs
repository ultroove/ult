﻿using System;
using System.Collections.Generic;

namespace Optimization
{
    class TestFunctions
    {
        //объявление делегата
        public delegate double FunctionDelegate(List<double> x);

        //метод для получения нужной функции по индексу
        public static FunctionDelegate GetFunction(int a)
        {
            var functions = new List<FunctionDelegate> { Q1, Q2, Q3, Q4, Q5, Q6 };
            return functions[a];
        }

        public static int GetMeasure(int a)
        {
            var measure = new List<int> { 2, 3, 2, 3, 4, 10 };
            return measure[a];
        }

        //метод для получения начальной точки по индексу функции
        public static List<double> GetDefaultPoint(int a)
        {
            var points = new List<List<double>>
            {
                new List<double> { 0, 1 },
                new List<double> { 0.5, 1, 0.5 },
                new List<double> { -1.2, 1 },
                new List<double> { 1, 0, 0 },
                new List<double> { 3, 1, 0, 1 },
                new List<double> { 100, 100, 100, 100, 100, 100, 100, 100, 100, 100 }
            };
            return points[a];
        }
        
        public static double Q1(List<double> x) //парабола, n=2
        {
            return Math.Pow(x[0] - x[1], 2) + Math.Pow(x[0] + x[1] - 10, 2) / 9;
        }

        public static double Q2(List<double> x) //парабола, n=3
        {
            return Math.Pow(x[0] - x[1] + x[2], 2) + Math.Pow(-x[0] + x[1] + x[2], 2) + Math.Pow(x[0] + x[1] - x[2], 2);
        }

        public static double Q3(List<double> x) //функция Розенброка
        {
            return 100 * Math.Pow(x[1] - x[0] * x[0], 2) + Math.Pow(1 - x[0], 2);
        }

        public static double Q4(List<double> x) //асимметричная долина
        {
            double a, b;
            if (x[0] > 0)
                a = 1 / (2 * Math.PI) - Math.Atan(x[1] / x[0]);
            else // (x[0] < 0)
                a = 1 / (2 * Math.PI) * (Math.PI + Math.Atan(x[1] / x[0]));
            b = Math.Sqrt(x[0] * x[0] + x[1] * x[1]);
            return 100 * (Math.Pow(x[2] - 10 * a, 2) + Math.Pow(b - 1, 2)) + x[2] * x[2];
        }

        public static double Q5(List<double> x) //функция Пауэлла
        {
            return Math.Pow(x[0] + 10 * x[1], 2) + 5 * Math.Pow(x[2] - x[3], 2) + Math.Pow(x[1] - 2 * x[2], 4) +
                   10 * Math.Pow(x[0] - x[3], 4);
        }

        public static double Q6(List<double> x) //квадратичная функция
        {
            double s = 0;
            for (var i = 0; i < x.Count; i++)
                s += Math.Pow(x[i] - (i + 1), 2);
            return s;
        }
    }
}
